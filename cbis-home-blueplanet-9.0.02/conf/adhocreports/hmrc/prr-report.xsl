<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns="http://www.govtalk.gov.uk/taxation/PSOnline/PensionRegulatorReport/1">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="prr">
        <PensionRegulatorReport>                   
            
            <xsl:apply-templates select="Header"/>
            
            <xsl:apply-templates select="Product"/>
           
            
            <Footer>
                <xsl:apply-templates select="Footer"/>
            </Footer>
        </PensionRegulatorReport>
    </xsl:template>

    <xsl:template match="Header">
	<Header>
		<xsl:call-template name="copyAll"/> 
	</Header>   
    </xsl:template>
    
    <xsl:template match="Product">
        <Product>
		<xsl:copy-of select="product_type_id"/>
		<xsl:copy-of select="product_name"/>
		<xsl:copy-of select="pso_ref_no"/>     
		<xsl:if test="Employer">
			<xsl:apply-templates select="Employer"/>
		</xsl:if>   
	 </Product>
    </xsl:template>
    
    <xsl:template match="Employer">
        <Employer>
		<xsl:if test="employer_crn">
		   <xsl:copy-of select="employer_crn"/>
		</xsl:if>
		<xsl:copy-of select="employer_name"/>
		<xsl:copy-of select="employer_id"/>
		<xsl:copy-of select="employer_address"/>		
		<xsl:if test="Member">
		   <xsl:apply-templates select="Member"/>
		</xsl:if>
	</Employer>
    </xsl:template>


    <xsl:template match="Member">
        <Member>
		<xsl:copy-of select="forename"/>
		<xsl:copy-of select="surname"/>
		<xsl:copy-of select="product_type_id"/>
		<xsl:copy-of select="title"/>
		<xsl:copy-of select="member_id"/>
		<xsl:copy-of select="member_name"/>
		<xsl:copy-of select="requested_name"/>
		<xsl:copy-of select="employer_id"/>
		<xsl:copy-of select="member_nino"/>
		<xsl:copy-of select="member_account_id"/>

		<xsl:if test="ExpectedContribution">
			<xsl:apply-templates select="ExpectedContribution"/>
		</xsl:if>
	</Member>
     </xsl:template>

     <xsl:template match="ExpectedContribution">
        <ExpectedContribution>
		<xsl:copy-of select="cont_type"/>
		<xsl:copy-of select="frequency"/>
		<xsl:copy-of select="due_date"/>
		<xsl:copy-of select="due_amount"/>
		<xsl:copy-of select="currency"/>
		<xsl:if test="ReceivedContribution">
		   <xsl:for-each select="ReceivedContribution">
			<xsl:copy-of select="payment_date"/>
		   </xsl:for-each>
		</xsl:if>
		<xsl:if test="ReceivedContribution">
			<xsl:for-each select="ReceivedContribution">
				<xsl:copy-of select="payment_amount"/>
				<xsl:copy-of select="currency"/>)
			</xsl:for-each>
		</xsl:if>
	</ExpectedContribution>
    </xsl:template>


    <xsl:template match="Footer">
	    <xsl:call-template name="copyAll"/>
    </xsl:template>

    <!-- *********** common function *************** -->
    <xsl:template name="copyAll">
        <xsl:for-each select="*">
            <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
