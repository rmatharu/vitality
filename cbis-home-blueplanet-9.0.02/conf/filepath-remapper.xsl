<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Description: this XSLT is designed to be a generic substitution transformation for CBISRequest messages.
    The idea is that the user defines path remapping config in the pathMappings variable. 
    The variable will contain a:
    1. from element : represents a string at the start of an element in the CBISRequest message that is being transformed.
    and  
    2. to element : represents a string that will substitute the substring matching the from element which was found in the CBISRequest message.
    
    This script will transform all occurrences in a given CBISRequest message as outlined by the template matching
    sequence below.  NOTE: if you wish to add additional messages simply add templates which go down to the
    appropriate level and call the sub-values template with the appropriate values.
    
    Due to the restrictions of functional programming in XSLT the first positive substitution is used in the pathMappings.
    This means that no further substitutions will be made on the given target element in the normalised message. If no
    pathMapping entry matches the target element then it is used unchanged in the transformed message.
    
    @author Adrian Silveri
    @author Ramon Buckland
    @author Vishal Puri
    @author Takuma Ueda		09/09/2009	Ported across from CBIS 2.4.x. Changed to work on CBISRequest rather than Normalised Messages.
    @author Takuma Ueda		11/11/2009	Modified to handle new CBISRequest format.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cr="http://www.infocomp.com/cbis/common/request/1.0" xmlns:exsl="http://exslt.org/common"
    exclude-result-prefixes="exsl" version="1.0">

    <xsl:output method="xml" indent="yes"/>

    <!-- ============================================================ -->
    <!-- Configuration between these lines can be changed             -->
    <!-- ============================================================ -->
    <!-- 
        | Valid values are windows, or unix.
        | If the value is unix, all \ will be changed to / after it has been "prefix" converted
    -->
    <xsl:param name="CBIS_SERVER_OS" select="'windows'"/>

    <!-- 
        |  Add new entries to the config below.
        |  for each new drive mapping
        |  Provide both upper and lowercase remappings
    -->
    <xsl:variable name="pathMappings">
        <config>
            <entry>
                <from>X:\</from>
                <to></to>
            </entry>
            <entry>
                <from>x:\</from>
                <to></to>
            </entry>
        </config>
    </xsl:variable>
    <!-- ============================================================ -->

    <!--  
        This is the main template that is used to transform the CBISRequest.
        
        Note: DO NOT add a slash "/" infront of cr:CBISRequest in the match here.
        Transforming interceptor in Spring WS doesn't work with a slash.
    -->
    <xsl:template match="cr:CBISRequest">
        <CBISRequest xmlns="http://www.infocomp.com/cbis/common/request/1.0">
            <!--
                Simply copy everything except arguments.
            -->
            <xsl:apply-templates select="child::*[not(local-name()='arguments')]"/>
            <arguments>
                <xsl:for-each select="child::*[local-name()='arguments']/*[local-name()='argument']">
                    <argument>
                        <xsl:apply-templates select="cr:key"/>
                        <xsl:apply-templates select="cr:value" mode="replace"/>
                    </argument>
                </xsl:for-each>
            </arguments>
        </CBISRequest>
    </xsl:template>

    <!-- Copies all attributes and node -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- Copies all attributes and node by replacing path -->
    <xsl:template match="@*|node()" mode="replace">
        <xsl:variable name="elemName" select="name()"/>
        <xsl:element name="{$elemName}" namespace="{namespace-uri(.)}">
            <xsl:variable name="textValue" select="."/>

            <xsl:choose>
                <!--  if there is a node that matches -->
                <xsl:when
                    test="exsl:node-set($pathMappings)/config/entry[starts-with($textValue,from)]">
                    <!--  get the newpathprefix -->
                    <xsl:variable name="newPathPrefix"
                        select="exsl:node-set($pathMappings)/config/entry[starts-with($textValue,from)]/to"/>
                    <!--  get the old path prefix -->
                    <xsl:variable name="oldPathPrefix"
                        select="exsl:node-set($pathMappings)/config/entry[starts-with($textValue,from)]/from"/>
                    <!--  and concat / substring them together -->
                    <xsl:choose>
                        <xsl:when test="$CBIS_SERVER_OS = 'unix'">
                            <xsl:value-of
                                select="translate(concat($newPathPrefix,substring-after($textValue,$oldPathPrefix)),'\','/')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="translate(concat($newPathPrefix,substring-after($textValue,$oldPathPrefix)),'/','\')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
