<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"    
    xmlns="http://www.govtalk.gov.uk/CM/envelope" >
    <xsl:output method="xml" indent="yes" />
    <xsl:template match="hmrc">
        <!-- <xsl:copy-of select="."/>  IDEM transformer -->
        <GovTalkMessage>
            <EnvelopeVersion>2.0</EnvelopeVersion>
            <xsl:apply-templates select="EasConstants"/>   <!-- these items are once per entire message -->
            <Body>
                <IRenvelope xmlns="http://www.govtalk.gov.uk/taxation/PAYE/RTI/EmployerAlignmentSubmission/3">
                    <IRheader>
                        <Keys>
                            <Key Type="TaxOfficeNumber"><xsl:value-of select="EasConstants/tax_office_code"/></Key>
                            <Key Type="TaxOfficeReference"><xsl:value-of select="EasConstants/paye_reference"/></Key>
                        </Keys>
                        <PeriodEnd>
                            <xsl:value-of select="substring(EasConstants/period_end,0,11)"/>
                        </PeriodEnd>
                        <DefaultCurrency>GBP</DefaultCurrency>
                        <IRmark Type="generic">ZbJSKtKSjhAgKt9/c5i3GbiT0G4=</IRmark>
                        <Sender>Employer</Sender>                        
                    </IRheader>
                    <EmployerAlignmentSubmission>
                        <EmpRefs>
                            <OfficeNo><xsl:value-of select="EasConstants/tax_office_code"/></OfficeNo>
                            <PayeRef><xsl:value-of select="EasConstants/paye_reference"/></PayeRef>
                            <AORef><xsl:value-of select="EasConstants/emp_accts_office_ref"/></AORef>
                        </EmpRefs>
                        <NumberOfParts><xsl:value-of select="EasConstants/number_of_parts"/></NumberOfParts>
                        <xsl:if test="boolean(part_id[string-length(text())>0])">
                    		<UniquePartID>
                        		<xsl:value-of select="part_id"/>
                    		</UniquePartID>
                		</xsl:if>                                            
                        <xsl:for-each select="EmployeeRecord">
                            <xsl:call-template name="OneEmployee"></xsl:call-template>
                        </xsl:for-each>
                    </EmployerAlignmentSubmission>   
                </IRenvelope>
            </Body>
        </GovTalkMessage>
    </xsl:template>
    
    <xsl:template match="EasConstants" >
        <xsl:element name="Header" >
            <MessageDetails>
            	<xsl:if test="test_submission = 1">
            		<Class>HMRC-PAYE-RTI-EAS-TIL</Class>
            	</xsl:if>
            	<xsl:if test="test_submission != 1">
            		<Class>HMRC-PAYE-RTI-EAS</Class>
            	</xsl:if>            	
                <Qualifier>request</Qualifier>
                <Function>submit</Function>
                <TransactionID><xsl:value-of select="batch_run_id"/></TransactionID>
                <!--  the CorrelationID must be in the right place, with a child of an empty Text node -->
                <CorrelationID></CorrelationID>                
                <Transformation>XML</Transformation>
                	<!--  set GatewayTest to a constant, which will be replaced later, depending on the URL configured in CBIS -->
                <GatewayTest>2</GatewayTest>
            </MessageDetails>
            <SenderDetails>
                <IDAuthentication>
                    <SenderID><xsl:value-of select="sender_id"/></SenderID>
                    <Authentication>
                        <Method>clear</Method>
                        <Role>principal</Role>
                        <Value><xsl:value-of select="authentication"/></Value>
                    </Authentication>
                </IDAuthentication>
            </SenderDetails>
        </xsl:element>
        <GovTalkDetails>
            <Keys>
                <Key Type="TaxOfficeNumber"><xsl:value-of select="tax_office_code"/></Key>
                <Key Type="TaxOfficeReference"><xsl:value-of select="paye_reference"/></Key>
            </Keys>
            <ChannelRouting>
                <Channel>
                    <URI>1800</URI>  <!-- vendor ID can be hard wired -->
                    <Product><xsl:value-of select="product_name"/></Product>
                    <Version><xsl:value-of select="product_version"/></Version>
                </Channel>
                <Timestamp>
                    <xsl:value-of select="concat( substring(time_right_now,0,11), 'T',substring(time_right_now,12,12))"/> 
                </Timestamp>
            </ChannelRouting>
        </GovTalkDetails>
        
    </xsl:template>
    
    <xsl:template name="OneEmployee">
        <Employee xmlns="http://www.govtalk.gov.uk/taxation/PAYE/RTI/EmployerAlignmentSubmission/3">
            <EmployeeDetails>
                <xsl:if test="boolean(tax_file_number[string-length(text())>0])">
                    <NINO>
                        <xsl:value-of select="tax_file_number"/>
                    </NINO>
                </xsl:if>                    
                <Name>
                    <xsl:if test="boolean(title_name[string-length(text())>0])">
                        <Ttl>
                            <xsl:value-of select="title_name"/>
                        </Ttl>
                    </xsl:if>                    
                    <xsl:if test="boolean(given_name[string-length(text())>0])">
                        <Fore>
                            <xsl:value-of select="given_name"/>
                        </Fore>
                    </xsl:if>
                    <xsl:if test="boolean(second_name[string-length(text())>0])">
                        <Fore>
                            <xsl:value-of select="second_name"/>
                        </Fore>
                    </xsl:if>                    
                    <xsl:if test="boolean(initial[string-length(text())>0])">
                        <Initials>
                            <xsl:value-of select="initial"/>
                        </Initials>
                    </xsl:if>
                    <xsl:if test="boolean(surname[string-length(text())>0])">
                        <Sur>
                            <xsl:value-of select="surname"/>
                        </Sur>
                    </xsl:if>
                    
                </Name>
                <Address>
                    <xsl:if test="boolean(address_line_1[string-length(text())>0])">
                        <Line>
                            <xsl:call-template name="CheckLeadingCharacter">
                                <xsl:with-param name="addressline">
                                    <xsl:value-of select="address_line_1"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </Line>
                    </xsl:if>
                    <xsl:if test="boolean(address_line_2[string-length(text())>0])">
                        <Line>
                            <xsl:call-template name="CheckLeadingCharacter">
                                <xsl:with-param name="addressline">
                                    <xsl:value-of select="address_line_2"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </Line>
                    </xsl:if>
                    <xsl:if test="boolean(address_line_3[string-length(text())>0])">
                        <Line>
                            <xsl:call-template name="CheckLeadingCharacter">
                                <xsl:with-param name="addressline">
                                    <xsl:value-of select="address_line_3"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </Line>
                    </xsl:if>
                    <xsl:if test="boolean(address_line_4[string-length(text())>0])">
                        <Line>
                            <xsl:call-template name="CheckLeadingCharacter">
                                <xsl:with-param name="addressline">
                                    <xsl:value-of select="address_line_4"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </Line>
                    </xsl:if>
                    <xsl:if test="boolean(postcode[string-length(text())>0])">
                        <UKPostcode>
                            <xsl:value-of select="postcode"/>
                        </UKPostcode>
                    </xsl:if>
                    <xsl:if test="boolean(countrycode[string-length(text())>0])">
                        <ForeignCountry>
                            <xsl:value-of select="countrycode"/>
                        </ForeignCountry>
                    </xsl:if>                    
                </Address>
                <xsl:if test="boolean(birth_date[string-length(text())>0])">
                    <BirthDate>
                        <xsl:value-of select="substring(birth_date,0,11)"/>
                    </BirthDate>
                </xsl:if>                                    
                <Gender><xsl:value-of select="gender"/></Gender>
            </EmployeeDetails>            
            <Employment>
                <xsl:if test="boolean(occ_pen_flag[string-length(text())>0])">
                    <OccPenInd>
                        <xsl:value-of select="occ_pen_flag"/>
                    </OccPenInd>
                </xsl:if>     
                <xsl:if test="boolean(start_date[string-length(text())>0])">
                    <Starter>
                        <StartDate>
                            <xsl:value-of select="substring(start_date,0,11)"/>
                        </StartDate>
                    </Starter>
                </xsl:if>                     
                <PayId>
                    <xsl:value-of select="hmrc_payroll_id"/>
                </PayId>
                <xsl:if test="boolean(pay_non_individual_flag[string-length(text())>0])">
                    <PaymentToANonIndividual>
                        <xsl:value-of select="pay_non_individual_flag"/>
                    </PaymentToANonIndividual>
                </xsl:if>                                                                           
                <xsl:if test="boolean(irregular_employment_flag[string-length(text())>0])">
                    <IrrEmp>
                        <xsl:value-of select="irregular_employment_flag"/>
                    </IrrEmp>
                </xsl:if>        
                <xsl:if test="boolean(fund_end_date[string-length(text())>0])">
                    <LeavingDate>
                        <xsl:value-of select="substring(fund_end_date,0,11)"/>
                    </LeavingDate>
                </xsl:if>                                    
                <Payment>
                    <xsl:choose>
                        <xsl:when test="boolean(tax_basis_non_cumulative[string-length(text())>0])">
                            <TaxCode BasisNonCumulative="yes">
                                <xsl:value-of select="tax_code"/>
                            </TaxCode>                                
                        </xsl:when>
                        <xsl:otherwise>
                            <TaxCode>
                                <xsl:value-of select="tax_code"/>
                            </TaxCode>                                
                        </xsl:otherwise>
                    </xsl:choose>                        
                </Payment>
                
            </Employment>
        </Employee>
        
    </xsl:template>

    <!--
        The first character of address line must be alphabetical or numerical character. Remove the character if it is not.
    -->
    <xsl:template name="CheckLeadingCharacter">
        <xsl:param name="addressline"/>
        <xsl:variable name="legal_characters" select="'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
        <xsl:choose>
            <xsl:when test="contains($legal_characters,substring($addressline, 1, 1))">
                <xsl:value-of select="$addressline"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="CheckLeadingCharacter">
                    <xsl:with-param name="addressline" select="substring($addressline,2)" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


</xsl:stylesheet>
