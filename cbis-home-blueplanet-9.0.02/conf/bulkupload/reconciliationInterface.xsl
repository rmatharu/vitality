<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
   	xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm"
   	xmlns:upload="http://www.infocomp.com/conductor/cases/business/upload"
    	version="1.0">
    
	<xsl:template match="/">
	 	<cm:CBISNM xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm">
	 		<xsl:copy-of select="/cm:CBISNM/cm:properties"/>
	 		<cm:payload>
	 			<reconciliation-status>
	    			<external-reference>
	    				<xsl:value-of select="/cm:CBISNM/cm:properties/external-reference|/cm:CBISNM/cm:properties/cm:external-reference"/>
	    			</external-reference>
	     			<xsl:choose>
	     				<xsl:when test="/cm:CBISNM/cm:properties/processSuccess">
	     					<status>OK</status>
	     				</xsl:when>	
	    				<xsl:otherwise>
	    					<status>ERROR</status>
							<fault-code>
								<!-- for file-middle-name:
								 | validation-response (validate error),
								 | transaction-details-<cbis-id>-faulted (DataUpdaterFault in process)
								 | P.S. fatalError is put in fault-code and other errors will be put in details, see following
								 -->
								<xsl:apply-templates select="/cm:CBISNM/cm:payload/fatalError|/cm:CBISNM/cm:payload/cm:fatalError"/>
								<!-- for file-middle-name:
								 | validation-response (validate error),
								 | transaction-details-<cbis-id>-faulted (DataUpdaterFault)
								 | P.S. when there is no fatalError, we put errors in fault-code
								 -->							
								<xsl:apply-templates select="/cm:CBISNM/cm:payload/error|/cm:CBISNM/cm:payload/cm:error[count(cm:CBISNM/cm:payload/fatalError|/cm:CBISNM/cm:payload/cm:fatalError)&lt;1]" mode="faultcode"/>
								<!-- 
								 | Conductor fault with file-middle-name as:
								 | upload-request-faulted (in process)
								 | investor-transaction-<cbis-id>-faulted (in process)
								 -->
								<xsl:apply-templates select="/cm:CBISNM/cm:properties/faultstring"/>
	    							<!-- for fault such as
								 | catch all in both process and validate (fatal error, database connection fault)
								 | catch by both process and validate (externalError)
								 | data retrieve error (catch by process)
								 -->
								<xsl:apply-templates select="/cm:CBISNM/cm:properties/cbis-spf-status-text" />
								<!-- for transaction association
								 | internal process error
								 -->
								<xsl:apply-templates select="//transactionAssociationRequest/error_message" />
								<xsl:apply-templates select="//ExternalError" />
							</fault-code>	
							<details>
								<!-- for file-middle-name:
								 | validation-response (validate error),
								 | transaction-details-<cbis-id>-faulted (DataUpdaterFault)
								 | P.S. when there is fatalError, Errors will be put in details!
								 -->
	    						<xsl:apply-templates select="/cm:CBISNM/cm:payload/error|/cm:CBISNM/cm:payload/cm:error[count(cm:CBISNM/cm:payload/fatalError|/cm:CBISNM/cm:payload/cm:fatalError) > 0]" mode="details"/>
	    						<!-- for fault such as
								 | catch all in both process and validate (fatal error, database connection fault)
								 | catch by both process and validate (externalError)
								 | data retrieve error (catch by process)
								 -->
								 <xsl:apply-templates select="/cm:CBISNM/cm:payload/log-entry/log-message"/>
								 <!--
								 | select the xml detail (we still want it to be in xml format not
								 | just the string concatenation of the text nodes
								 -->
								 <xsl:apply-templates select="//ExternalError/exception-details" />
							</details>
	    				</xsl:otherwise>
	     			</xsl:choose>
	 			</reconciliation-status>
	 		</cm:payload>
		</cm:CBISNM>
	</xsl:template>

	<xsl:template match="//ExternalError/fault-msg">
		<xsl:value-of select="text()"/>
	</xsl:template>
	
	<xsl:template match="//ExternalError/exception-details">
		<xsl:copy-of select="."/>
	</xsl:template>
	
	<xsl:template match="/cm:CBISNM/cm:payload/fatalError|/cm:CBISNM/cm:payload/cm:fatalError">
		<xsl:value-of select="text()"/>
	</xsl:template>
	
	<xsl:template match="/cm:CBISNM/cm:properties/cbis-spf-status-text">
		<xsl:value-of select="text()"/>
	</xsl:template>
	 	
 	<xsl:template match="/cm:CBISNM/cm:payload/error|/cm:CBISNM/cm:payload/cm:error" mode="faultcode">
 		<xsl:value-of select="text()"/>
 	</xsl:template>
	
	<xsl:template match="/cm:CBISNM/cm:payload/error|/cm:CBISNM/cm:payload/cm:error" mode="details">
		<xsl:copy-of select="."/>
	</xsl:template>
	
	<xsl:template match="/cm:CBISNM/cm:properties/faultstring">
		<xsl:value-of select="text()"/>
	</xsl:template>
		     				
	<xsl:template match="/cm:CBISNM/cm:payload/log-entry/log-message">
		<xsl:copy-of select="."/>
	</xsl:template>
	
	<xsl:template match="//transactionAssociationRequest/error_message">
		<xsl:value-of select="text()"/>
	</xsl:template>	  				
</xsl:stylesheet>
 
